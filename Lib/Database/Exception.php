<?php
/**
 * mFramework - a mini PHP framework
 * 
 * @package   mFramework
 * @version   v5
 * @copyright 2009-2016 Wynn Chen
 * @author	Wynn Chen <wynn.chen@outlook.com>
 */
namespace mFramework\Database;

/**
 * 数据库模块相关的Exception
 *
 * @package mFramework
 * @author Wynn Chen
 */
class Exception extends \mFramework\Exception
{}