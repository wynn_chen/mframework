<?php
/**
 * mFramework - a mini PHP framework
 * 
 * @package   mFramework
 * @version   v5
 * @copyright 2009-2016 Wynn Chen
 * @author	Wynn Chen <wynn.chen@outlook.com>
 */
namespace mFramework;

/**
 * 框架的基本异常。
 * 本框架的所有异常全部继承于此。
 *
 * @package mFramework
 * @author Wynn Chen
 */
class Exception extends \Exception
{}