<?php
/**
 * mFramework - a mini PHP framework
 * 
 * Require PHP 7 since v4.0
 *
 * @package   mFramework
 * @version   4.0
 * @copyright 2009 Wynn Chen
 * @author	Wynn Chen <wynn.chen@outlook.com>
 */
namespace mFramework\Html;

/**
 * Html相关的Exception
 *
 * @package mFramework
 * @author Wynn Chen
 */
class Exception extends \mFramework\Exception
{}