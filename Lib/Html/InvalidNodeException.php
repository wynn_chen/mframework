<?php
/**
 * mFramework - a mini PHP framework
 * 
 * Require PHP 7 since v4.0
 *
 * @package   mFramework
 * @version   4.0
 * @copyright 2009 Wynn Chen
 * @author	Wynn Chen <wynn.chen@outlook.com>
 */
namespace mFramework\Html;

/**
 * 节点无效。
 *
 * @package mFramework
 * @author Wynn Chen
 */
class InvalidNodeException extends Exception
{}