<?php
/**
 * mFramework - a mini PHP framework
 * 
 * @package   mFramework
 * @version   v5
 * @copyright 2009-2016 Wynn Chen
 * @author	Wynn Chen <wynn.chen@outlook.com>
 */
namespace mFramework\Html;

/**
 *
 * Text
 *
 * @package mFramework
 * @author Wynn Chen
 *		
 */
class Text extends \DOMText
{
	use NodeTrait;
}
