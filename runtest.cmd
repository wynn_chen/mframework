@echo off
echo ===============================================
echo ===============================================
echo =======                                 =======
echo =======   %DATE% %TIME%   =======
echo =======                                 =======
echo ===============================================
echo ===============================================
phpunit --coverage-html .\TestReport -c .\Tests\phpunit.xml